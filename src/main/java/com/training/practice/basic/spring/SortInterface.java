package com.training.practice.basic.spring;

public interface SortInterface {

    public Integer[] sort(Integer[] array);
}
