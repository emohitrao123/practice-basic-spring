package com.training.practice.basic.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeBasicSpringApplication {


	public static void main(String[] args) {

		//instance of the BinarySearch class
		BinarySearch bSearch = new BinarySearch(new QuickSort());

		Integer index = bSearch.binarySearch(new Integer[] {12,3,4,15}, 4);

		System.out.println("The index is "+index);
		SpringApplication.run(PracticeBasicSpringApplication.class, args);
	}

}
