package com.training.practice.basic.spring;

public class BinarySearch {

    private SortInterface sortInterface;

    public BinarySearch(SortInterface sortInterface) {
        this.sortInterface = sortInterface;
    }


    public Integer binarySearch(Integer[] array, Integer target){
        //sort the array

        /*BubbleSort bubbleSort = new BubbleSort();
        Integer[] sortedArray = bubbleSort.sort(array);*/

        Integer[] sortedArray = sortInterface.sort(array);
        System.out.println("Sorting used is "+sortInterface);

        //search the array
        //return the index
        Integer low = 0;
        Integer high = sortedArray.length -1;
        while(low < high){
            Integer mid = low + (high-low)/2;
            if(sortedArray[mid] == target)
                return mid;
            if(sortedArray[mid]< target){
                low = mid +1;
            }
            else high = mid -1;
        }
        return -1;
    }
}
